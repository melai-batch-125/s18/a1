//1
function addStudent(name) {
	student.push(name);
	console.log(`Added successfully : ${name}`)
}

//2
function printStudents() {
	console.log(`Below is the list of names added successfully:`)
	student.forEach(
		function(name) {
			console.log(name);
		})
}

//3
function countStudents() {
	console.log(`Total of students: ${student.length} students.`);
}

//4
function findStudent(name) {
	if (student.includes(name) == true) {
		console.log(`${name} is included in the list.`);
	} else {
		console.log(`${name} is not included in the list.`);
	}
	
}

let student = [];

addStudent(`Melai`);
addStudent(`Levy`);
addStudent(`Zack`);
printStudents();
countStudents();
findStudent(`Zack`);
findStudent(`Ezekiel`);
findStudent(`Eren`);


